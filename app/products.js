const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const auth = require('../auth');
const Product = require('../models/Product');
const User = require('../models/User');
const Category = require('../models/Category');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {

    // Product index
    router.get('/', async (req, res) => {
        const product = await Product.find().populate('user');
        const category = await Category.find();
        res.send({product, category});
    });

    // Product create
    router.post('/', upload.single('image'), (req, res) => {
        if(!req.body) {
            res.status(500).send('please fill all fields');
        }

        if(!req.body.user) {

        }

        const productData = req.body;

        if (!req.file) {
            res.status(500).send('you gave to post image');
        } else {
            productData.image = req.file.filename;
        }

        const product = new Product(productData);

        product.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    // Product get by ID
    router.get('/:id', (req, res) => {
        const id = req.params.id;
        Product.findOne({_id: id}).populate('category').populate('user')
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.get('/category/:id', async (req, res) => {
        const id = req.params.id;
        const product = await Product.find({category: id});
        const category = await Category.find();
        res.send({product, category});
    });

    router.delete('/:id', (req, res) => {
        const id = req.params.id;
        Product.remove({_id: id})
            .then(result => {
                res.send(result);
            })
    });

    return router;
};

module.exports = createRouter;