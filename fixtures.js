const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const User = require('./models/User');
const Product = require('./models/Product');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('categories');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [inokenti, admin, kaito] = await User.create(
        {
            username: 'inokenti',
            password: '123',
            displayName: 'inokenti',
            phoneNumber: '1234'
        },
        {
            username: 'admin',
            password: '123',
            displayName: 'admin',
            phoneNumber: '1234'
        },
        {
            username: 'kairat',
            password: '123',
            displayName: 'kaito',
            phoneNumber: '1234'
        }
    );

    const [carsCategory, computerCategory, foodCategory] = await Category.create(
        {
            title: 'Cars',
            description: 'Cars category'
        }, {
            title: 'Computers',
            description: 'Computers and notebooks category'
        },
        {
            title: 'Food',
            description: 'Food category'
        }
    );

    await Product.create({
            title: 'Audi',
            description: 'Sedan car',
            image: 'audi.png',
            price: 20,
            category: carsCategory._id,
            user: inokenti._id
        },
        {
            title: 'Macbook',
            description: 'Apple macbook',
            image: 'notebook.jpeg',
            price: 30,
            category: computerCategory._id,
            user: admin._id
        },
        {
            title: 'Lagman',
            description: 'Lapsha and myaso',
            image: 'lagman.jpg',
            price: 50,
            category: foodCategory._id,
            user: kaito._id
        }
    );

    db.close();
});